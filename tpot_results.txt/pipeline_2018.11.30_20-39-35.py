import numpy as np
import pandas as pd
from sklearn.ensemble import ExtraTreesRegressor, GradientBoostingRegressor
from sklearn.model_selection import train_test_split
from sklearn.pipeline import make_pipeline, make_union
from sklearn.preprocessing import Normalizer
from sklearn.svm import LinearSVR
from tpot.builtins import StackingEstimator

# NOTE: Make sure that the class is labeled 'target' in the data file
tpot_data = pd.read_csv('PATH/TO/DATA/FILE', sep='COLUMN_SEPARATOR', dtype=np.float64)
features = tpot_data.drop('target', axis=1).values
training_features, testing_features, training_target, testing_target = \
            train_test_split(features, tpot_data['target'].values, random_state=99)

# Average CV score on the training set was:-0.4720685485285004
exported_pipeline = make_pipeline(
    StackingEstimator(estimator=LinearSVR(C=25.0, dual=False, epsilon=0.001, loss="squared_epsilon_insensitive", tol=0.001)),
    Normalizer(norm="l2"),
    StackingEstimator(estimator=ExtraTreesRegressor(bootstrap=True, max_features=0.6000000000000001, min_samples_leaf=18, min_samples_split=6, n_estimators=100)),
    GradientBoostingRegressor(alpha=0.85, learning_rate=0.1, loss="ls", max_depth=9, max_features=0.05, min_samples_leaf=15, min_samples_split=6, n_estimators=100, subsample=0.7000000000000001)
)

exported_pipeline.fit(training_features, training_target)
results = exported_pipeline.predict(testing_features)
