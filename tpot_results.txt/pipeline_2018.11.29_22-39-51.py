import numpy as np
import pandas as pd
from sklearn.ensemble import GradientBoostingRegressor, RandomForestRegressor
from sklearn.linear_model import LassoLarsCV
from sklearn.model_selection import train_test_split
from sklearn.pipeline import make_pipeline, make_union
from sklearn.preprocessing import Normalizer
from tpot.builtins import StackingEstimator

# NOTE: Make sure that the class is labeled 'target' in the data file
tpot_data = pd.read_csv('PATH/TO/DATA/FILE', sep='COLUMN_SEPARATOR', dtype=np.float64)
features = tpot_data.drop('target', axis=1).values
training_features, testing_features, training_target, testing_target = \
            train_test_split(features, tpot_data['target'].values, random_state=399)

# Average CV score on the training set was:-0.9470313502610999
exported_pipeline = make_pipeline(
    StackingEstimator(estimator=RandomForestRegressor(bootstrap=True, max_features=0.55, min_samples_leaf=16, min_samples_split=20, n_estimators=100)),
    StackingEstimator(estimator=GradientBoostingRegressor(alpha=0.99, learning_rate=0.5, loss="quantile", max_depth=6, max_features=0.6000000000000001, min_samples_leaf=19, min_samples_split=7, n_estimators=100, subsample=0.45)),
    Normalizer(norm="l2"),
    LassoLarsCV(normalize=True)
)

exported_pipeline.fit(training_features, training_target)
results = exported_pipeline.predict(testing_features)
