import numpy as np
import pandas as pd
from sklearn.decomposition import PCA
from sklearn.linear_model import RidgeCV
from sklearn.model_selection import train_test_split
from sklearn.neighbors import KNeighborsRegressor
from sklearn.pipeline import make_pipeline, make_union
from tpot.builtins import OneHotEncoder, StackingEstimator

# NOTE: Make sure that the class is labeled 'target' in the data file
tpot_data = pd.read_csv('PATH/TO/DATA/FILE', sep='COLUMN_SEPARATOR', dtype=np.float64)
features = tpot_data.drop('target', axis=1).values
training_features, testing_features, training_target, testing_target = \
            train_test_split(features, tpot_data['target'].values, random_state=999)

# Average CV score on the training set was:-0.23391775174234725
exported_pipeline = make_pipeline(
    StackingEstimator(estimator=RidgeCV()),
    OneHotEncoder(minimum_fraction=0.05, sparse=False, threshold=10),
    PCA(iterated_power=5, svd_solver="randomized"),
    KNeighborsRegressor(n_neighbors=33, p=2, weights="distance")
)

exported_pipeline.fit(training_features, training_target)
results = exported_pipeline.predict(testing_features)
