import numpy as np
import pandas as pd
from sklearn.ensemble import RandomForestRegressor
from sklearn.linear_model import RidgeCV
from sklearn.model_selection import train_test_split
from sklearn.pipeline import make_pipeline, make_union
from sklearn.preprocessing import Normalizer, StandardScaler
from tpot.builtins import StackingEstimator
from xgboost import XGBRegressor

# NOTE: Make sure that the class is labeled 'target' in the data file
tpot_data = pd.read_csv('PATH/TO/DATA/FILE', sep='COLUMN_SEPARATOR', dtype=np.float64)
features = tpot_data.drop('target', axis=1).values
training_features, testing_features, training_target, testing_target = \
            train_test_split(features, tpot_data['target'].values, random_state=399)

# Average CV score on the training set was:-1.0581048040711731
exported_pipeline = make_pipeline(
    Normalizer(norm="l1"),
    StackingEstimator(estimator=RandomForestRegressor(bootstrap=False, max_features=0.7500000000000001, min_samples_leaf=4, min_samples_split=13, n_estimators=100)),
    StackingEstimator(estimator=XGBRegressor(learning_rate=1.0, max_depth=10, min_child_weight=2, n_estimators=100, nthread=1, subsample=0.15000000000000002)),
    StandardScaler(),
    RidgeCV()
)

exported_pipeline.fit(training_features, training_target)
results = exported_pipeline.predict(testing_features)
