import numpy as np
import pandas as pd
from sklearn.decomposition import PCA
from sklearn.feature_selection import SelectFwe, f_regression
from sklearn.kernel_approximation import Nystroem
from sklearn.linear_model import RidgeCV
from sklearn.model_selection import train_test_split
from sklearn.neighbors import KNeighborsRegressor
from sklearn.pipeline import make_pipeline, make_union
from sklearn.preprocessing import PolynomialFeatures
from tpot.builtins import StackingEstimator

# NOTE: Make sure that the class is labeled 'target' in the data file
tpot_data = pd.read_csv('PATH/TO/DATA/FILE', sep='COLUMN_SEPARATOR', dtype=np.float64)
features = tpot_data.drop('target', axis=1).values
training_features, testing_features, training_target, testing_target = \
            train_test_split(features, tpot_data['target'].values, random_state=399)

# Average CV score on the training set was:-0.933852473808156
exported_pipeline = make_pipeline(
    StackingEstimator(estimator=RidgeCV()),
    Nystroem(gamma=0.35000000000000003, kernel="cosine", n_components=2),
    PolynomialFeatures(degree=2, include_bias=False, interaction_only=False),
    PCA(iterated_power=1, svd_solver="randomized"),
    SelectFwe(score_func=f_regression, alpha=0.037),
    StackingEstimator(estimator=RidgeCV()),
    KNeighborsRegressor(n_neighbors=10, p=2, weights="distance")
)

exported_pipeline.fit(training_features, training_target)
results = exported_pipeline.predict(testing_features)
