import numpy as np
import pandas as pd
from sklearn.kernel_approximation import Nystroem
from sklearn.linear_model import LassoLarsCV
from sklearn.model_selection import train_test_split
from sklearn.pipeline import make_pipeline, make_union
from sklearn.preprocessing import StandardScaler
from tpot.builtins import StackingEstimator
from xgboost import XGBRegressor

# NOTE: Make sure that the class is labeled 'target' in the data file
tpot_data = pd.read_csv('PATH/TO/DATA/FILE', sep='COLUMN_SEPARATOR', dtype=np.float64)
features = tpot_data.drop('target', axis=1).values
training_features, testing_features, training_target, testing_target = \
            train_test_split(features, tpot_data['target'].values, random_state=399)

# Average CV score on the training set was:-0.6852396611966858
exported_pipeline = make_pipeline(
    Nystroem(gamma=0.8500000000000001, kernel="cosine", n_components=1),
    StackingEstimator(estimator=LassoLarsCV(normalize=True)),
    StandardScaler(),
    StackingEstimator(estimator=XGBRegressor(learning_rate=0.001, max_depth=7, min_child_weight=10, n_estimators=100, nthread=1, subsample=0.7500000000000001)),
    Nystroem(gamma=0.8500000000000001, kernel="cosine", n_components=1),
    XGBRegressor(learning_rate=1.0, max_depth=5, min_child_weight=17, n_estimators=100, nthread=1, subsample=0.9500000000000001)
)

exported_pipeline.fit(training_features, training_target)
results = exported_pipeline.predict(testing_features)
